# OMS Krumo

A WordPress plugin that adds Krumo debugging to your WordPress project. To learn more about Krumo, [click here](http://krumo.sourceforge.net).

**Krumo Excerpt from Sourceforge:**

"A lot of developers use print_r() and var_dump() in the means of debugging tools. Although they were intended to present human readable information about a variable, we can all agree that in general they are not. Krumo is an alternative: it does the same job, but it presents the information beautified using CSS and DHTML."

## Installation

OMS Krumo is a WordPress plugin. To install, simply create a folder named `oms-krumo` in your plugins directory and copy the files in this repository to it.

## Inline Debugging

To debug a variable from anywhere in your code, type:

```
$var = 'My String Literal';
omsk( $var );
```

If you're familiar with the `Devel` module for Drupal then you may want to use `kpr()` instead:

```
$var = [ 'Orange', 'Banana', 'Apple' ];
kpr( $var );
```

## Delayed Debugging

To debug a variable from anywhere in your code (even Ajax) and have it output on the next page load, type:

```
$var = 999999999;
omsdk( $var );
```

**Note:** Delayed debugging relies on the PHP session. Please make sure the session is set in the scope that you're calling `omsdk()`.

**Note:** Delayed debugging doesn't automatically output. You need to paste the following in your `header.php` file (or wherever you want debugging to appear):

```
<?php if ( function_exists( 'oms_krumo_has_output' ) && oms_krumo_has_output() ) : ?>
    <?php oms_krumo_output(); ?>
<?php endif; ?>
```