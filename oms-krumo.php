<?php

/**
 * Plugin Name: OMS Krumo
 * Plugin URI: http://www.orbitmedia.com
 * Description: Debuging for WordPress using Krumo.
 * Version: 0.0.1
 * Author: Orbit Media Studios
 * Author: Jimmy K. <jimmy@orbitmedia.com>
 * Author URI: http://www.orbitmedia.com
 */

if ( session_id() == '' ) {
	// Initialize the session if it's not already initialized.
	session_start();
}

// Include the Krumo class.
require_once( plugin_dir_path( __FILE__ ) . 'krumo/class.krumo.php' );

// ============================== //
// Actions
// ============================== //

/**
 * Adds our custom CSS to make sure Krumo is still legible.
 *
 * @see add_action()
 *
 * @author Orbit Media <wordpress@orbitmedia.com>
 * @author Jimmy K. <jimmy@orbitmedia.com>
 */
function oms_krumo_init() {
	wp_enqueue_style( 'oms-krumo', plugins_url( 'resources/css/oms-krumo.css', __FILE__ ) );
}

add_action( 'init', 'oms_krumo_init' );

/**
 * Makes sure this plugin loads first. WordPress loads plugins alphabetically,
 * which makes sense, but our plugin should load before other plugins so it can
 * be used anywhere.
 *
 * @see add_action()
 *
 * @author Orbit Media <wordpress@orbitmedia.com>
 * @author Jimmy K. <jimmy@orbitmedia.com>
 */
function oms_krumo_change_load_order() {
	$path = str_replace( WP_PLUGIN_DIR . '/', '', __FILE__ );

	if ( $plugins = get_option( 'active_plugins' ) ) {
		if ( $key = array_search( $path, $plugins ) ) {
			array_splice( $plugins, $key, 1 );
			array_unshift( $plugins, $path );
			update_option( 'active_plugins', $plugins );
		}
	}
}

add_action( 'activated_plugin', 'oms_krumo_change_load_order' );

// ============================== //
// Functions
// ============================== //

/**
 * Outputs the Krumo log. Only required if using omsdk().
 *
 * @see omsdk()
 *
 * @author Orbit Media <wordpress@orbitmedia.com>
 * @author Jimmy K. <jimmy@orbitmedia.com>
 */
function oms_krumo_output() {
	echo '<div id="oms-krumo-output">';

	foreach ( $_SESSION['oms']['krumo'] as $value ) {
		// Loop through each debug item and add it to the output.
		krumo( $value );
	}

	echo '</div><!-- /#oms-krumo-output -->';

	// Unset the session values.
	oms_krumo_destroy();

	echo $output;
}

/**
 * Checks whether or not there is output.
 *
 * @return int
 *
 * @author Orbit Media <wordpress@orbitmedia.com>
 * @author Jimmy K. <jimmy@orbitmedia.com>
 */
function oms_krumo_has_output() {
	return isset( $_SESSION['oms']['krumo'] ) && count( $_SESSION['oms']['krumo'] ) > 0 ? TRUE : FALSE;
}

/**
 * Destroys the output session vars.
 *
 * @author Orbit Media <wordpress@orbitmedia.com>
 * @author Jimmy K. <jimmy@orbitmedia.com>
 */
function oms_krumo_destroy() {
	$_SESSION['oms']['krumo'] = [ ];
}

/**
 * Outputs the specified value using Krumo.
 *
 * @param $value
 *
 * @author Orbit Media <wordpress@orbitmedia.com>
 * @author Jimmy K. <jimmy@orbitmedia.com>
 */
function oms_krumo( $value ) {
	echo '<div id="oms-krumo-output">';
	krumo( $value );
	echo '</div><!-- /#oms-krumo-output -->';
}

/**
 * Outputs the specified value on the next page load (delayed) using Krumo.
 *
 * @param $value
 *
 * @author Orbit Media <wordpress@orbitmedia.com>
 * @author Jimmy K. <jimmy@orbitmedia.com>
 */
function oms_krumo_delayed( $value ) {
	$_SESSION['oms']['krumo'][] = $value;
}

// ============================== //
// Shorthand Functions
// ============================== //

/**
 * @see oms_krumo()
 *
 * @param $value
 *
 * @author Orbit Media <wordpress@orbitmedia.com>
 * @author Jimmy K. <jimmy@orbitmedia.com>
 */
function omsk( $value ) {
	oms_krumo( $value );
}

/**
 * Alias for Devel's kpr().
 *
 * @see oms_krumo()
 *
 * @param $value
 *
 * @author Orbit Media <wordpress@orbitmedia.com>
 * @author Jimmy K. <jimmy@orbitmedia.com>
 */
if ( ! function_exists( 'kpr' ) ) {
	// Function doesn't already exist, create it.
	function kpr( $value ) {
		oms_krumo( $value );
	}
}

/**
 * @see oms_krumo_delayed()
 *
 * @param $value
 *
 * @author Orbit Media <wordpress@orbitmedia.com>
 * @author Jimmy K. <jimmy@orbitmedia.com>
 */
function omsdk( $value ) {
	oms_krumo_delayed( $value );
}

/**
 * Alias for Devel's dpm().
 *
 * @see oms_krumo_delayed()
 *
 * @param $value
 *
 * @author Orbit Media <wordpress@orbitmedia.com>
 * @author Jimmy K. <jimmy@orbitmedia.com>
 */
if ( ! function_exists( 'dpm' ) ) {
	// Function doesn't already exist, create it.
	function dpm( $value ) {
		oms_krumo_delayed( $value );
	}
}
